﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FailingEndpoint.Messages.Commands
{
    public sealed class DoSomethingThatAlwaysFails
    {
        public Guid Id { get; set; }
    }
}
