﻿using System;
using System.Text;
using System.Threading.Tasks;
using FailingEndpoint.Messages.Commands;
using NServiceBus;
using FailingEndpoint.Monitor.Messages.Events;

namespace FailingEndpoint.Handlers
{
    public sealed class DoSomethingThatSometimesFailsHandler : IHandleMessages<DoSomethingThatSometimesFails>
    {
        public async Task Handle(DoSomethingThatSometimesFails message, IMessageHandlerContext context)
        {
            await Task.Run(() => Console.WriteLine($"Doing Something: {context.MessageId}"));
            Random rand = new Random();
            if (rand.Next(0, 5) != 4)
                throw new Exception("I'm failing");

            string sql = "SELECT MessageId FROM FailedMessages WHERE MessageId = @MessageId;";
            bool msgExists = await Program.Exists(context.MessageId, sql);

            if (msgExists)
            {
                sql = "DELETE FROM FailedMessages WHERE MessageId = @MessageId;";
                await Program.Delete(context.MessageId, sql);
                //Program.FailedMessages.Remove(context.MessageId);
                IAmARecoveredError err = new RecoveredAlertableError()
                {
                    Id = context.MessageId
                };
                await context.Publish(err).ConfigureAwait(false);
            }
        }
    }
}
