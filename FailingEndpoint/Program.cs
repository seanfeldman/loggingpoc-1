﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;
using FailingEndpoint.Messages.Commands;
using FailingEndpoint.Monitor.Messages.Events;
using MySql.Data.MySqlClient;
using NServiceBus;
using NServiceBus.Faults;
using NServiceBus.Persistence.Sql;

namespace FailingEndpoint
{
    class Program
    {
        //internal static Dictionary<string, string> FailedMessages = new Dictionary<string, string>();
        private static IEndpointInstance _endpointInstance;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            MainAsync().GetAwaiter().GetResult();
            return;
        }

        static async Task MainAsync()
        {
            Console.Title = "FailingEndpoint";

            if (_endpointInstance == null)
            {
                var endpointConfiguration = EndpointConfiguration();
                

                var endpointInstance = await Endpoint.Start(endpointConfiguration);
                _endpointInstance = endpointInstance;
            }
            //BusSession = endpointInstance;
            //await endpointInstance.ScheduleEvery(
            //    timeSpan: TimeSpan.FromSeconds(5),
            //    task: pipelineContext => pipelineContext.SendLocal(new DoSomethingThatAlwaysFails() {Id = Guid.NewGuid()})).ConfigureAwait(false);

            await CreateMessage.Loop(_endpointInstance);

            //await endpointInstance.SendLocal(new RefreshAatpFutureInventory());

            //await Task.Run(() => CheckStatus());

            Console.WriteLine("END");
            Console.ReadLine();

            await _endpointInstance.Stop();
        }

        private static EndpointConfiguration EndpointConfiguration()
        {
            var endpointConfiguration = new EndpointConfiguration("FailingEndpoint");
            endpointConfiguration.EnableInstallers();
            endpointConfiguration.SendFailedMessagesTo("Errors");
            endpointConfiguration.UseSerialization<JsonSerializer>();

            //endpointConfiguration.Notifications.Errors.MessageHasBeenSentToDelayedRetries += Errors_MessageHasBeenSentToDelayedRetries; ;
            endpointConfiguration.Notifications.Errors.MessageSentToErrorQueue += Errors_MessageSentToErrorQueue;
            ;
            endpointConfiguration.Notifications.Errors.MessageHasFailedAnImmediateRetryAttempt +=
                Errors_MessageHasFailedAnImmediateRetryAttempt;

            var persistence = endpointConfiguration.UsePersistence<SqlPersistence>();
            var connection = "server=xxxxxxxx;user=xxxxxxx;database=monitoringproto_prepod_persistence;port=3306;password=xxxxxx;AllowUserVariables=True;AutoEnlist=false";
            persistence.SqlVariant(SqlVariant.MySql);
            persistence.ConnectionBuilder(() => new MySqlConnection(connection));
            var subscriptions = persistence.SubscriptionSettings();
            subscriptions.CacheFor(TimeSpan.FromMinutes(1));

            //var recoverabilityConfiguration = endpointConfiguration.Recoverability();
            //recoverabilityConfiguration.AddUnrecoverableException<MessageValidationException>();

            var transport = endpointConfiguration.UseTransport<RabbitMQTransport>(); 
            transport.ConnectionString("host=xxxxxxxxx;username=xxxxxxxxxxxxxx;password=xxxxxxxxxxxxxx;virtualhost=xxxxxxxxxxxxxxx;UseTls=true;");

            //transport.Routing().RouteToEndpoint(typeof(UpdateAatpCurrentInventory), Configuration.ServiceBusCommandRoute);
            //transport.Routing().RouteToEndpoint(typeof(UpdateAatpFutureInventory), Configuration.ServiceBusCommandRoute);

            //endpointConfiguration.HeartbeatPlugin(serviceControlQueue: Properties.Settings.Default.ServiceControlInstance);

            return endpointConfiguration;
        }

        private static async void Errors_MessageHasFailedAnImmediateRetryAttempt(object sender, NServiceBus.Faults.ImmediateRetryMessage e)
        {
            string sql = "SELECT MessageId FROM FailedMessages WHERE MessageId = @MessageId;";
            bool msgExists = await Exists(e.MessageId, sql);

                if (!msgExists)
                {
                    //Program.FailedMessages.Add(e.MessageId, Encoding.UTF8.GetString(e.Body));
                sql = "INSERT INTO FailedMessages (MessageId, EndpointName) VALUES (@MessageId, @EndpointName);";
                await Insert(e.MessageId, sql);

                    IAmAnAlertableError err = new FailingEndpointAlertableError()
                    {
                        Id = e.MessageId,
                        Message = Encoding.UTF8.GetString(e.Body)
                    };

                    if (_endpointInstance == null)
                    {
                        var endpointConfiguration = EndpointConfiguration();


                        var endpointInstance = await Endpoint.Start(endpointConfiguration);
                        _endpointInstance = endpointInstance;
                    }
                    await _endpointInstance.Publish(err).ConfigureAwait(false);
                }
        }

        public static async Task Insert(string messageId, string sql)
        {
            sql = "INSERT INTO FailedMessages (MessageId, EndpointName) VALUES (@MessageId, @EndpointName);";
            using (
                var connection =
                    new MySqlConnection("server=xxxxxxxx;user=xxxxxxx;database=monitoringproto_prepod_persistence;port=3306;password=xxxxxx;AllowUserVariables=True;AutoEnlist=false")
            )
            {
                connection.Open();
                var result = await connection.ExecuteAsync(sql, new {MessageId = messageId, EndpointName = "FailingEndpoint"});
            }
        }

        public static async Task Delete(string messageId, string sql)
        {
            using (
                var connection =
                    new MySqlConnection("server=xxxxxxxx;user=xxxxxxx;database=monitoringproto_prepod_persistence;port=3306;password=xxxxxx;AllowUserVariables=True;AutoEnlist=false")
            )
            {
                connection.Open();
                var result = await connection.ExecuteAsync(sql, new { MessageId = messageId, EndpointName = "FailingEndpoint" });
            }
        }

        public static async Task<bool> Exists(string messageId, string sql)
        {
            using (
                var connection =new MySqlConnection("server=xxxxxxxx;user=xxxxxxx;database=monitoringproto_prepod_persistence;port=3306;password=xxxxxx;AllowUserVariables=True;AutoEnlist=false")
            )
            {
                connection.Open();
                var result = await connection.QueryFirstOrDefaultAsync<string>(sql, new {MessageId = messageId });
                return result != null;
            }
        }

        //private static void Errors_MessageHasBeenSentToDelayedRetries(object sender, NServiceBus.Faults.DelayedRetryMessage e)
        //{
        //    throw new NotImplementedException();
        //}

        private static async void Errors_MessageSentToErrorQueue(object sender, NServiceBus.Faults.FailedMessage e)
        {
            string sql = "DELETE FROM FailedMessages WHERE MessageId = @MessageId;";
            await Delete(e.MessageId, sql);

            //Program.FailedMessages.Remove(e.MessageId);
            Console.WriteLine("Message sent to error queue");
        }
    }
}
