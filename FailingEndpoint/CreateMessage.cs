﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FailingEndpoint.Handlers;
using FailingEndpoint.Messages.Commands;
using NServiceBus;
using NServiceBus.Logging;

namespace FailingEndpoint
{
    internal sealed class CreateMessage
    {
        private static readonly ILog _logger = LogManager.GetLogger<Program>();

        internal static async Task Loop(IMessageSession messageSession)
        {
            while (true)
            {
                Console.WriteLine();
                _logger.Warn("Press N to create a Failure Message, G to create a recoverable message or Q to quit.");

                var key = Console.ReadKey();
                Console.WriteLine();

                switch (key.Key)
                {
                    case ConsoleKey.N:
                        var doMsg = new DoSomethingThatAlwaysFails() { Id = Guid.NewGuid() };

                        await messageSession.SendLocal(doMsg).ConfigureAwait(false);
                        _logger.Info($"Created Command To Do Something With Id: {doMsg.Id}.");
                        break;
                    case ConsoleKey.G:
                        var doFail = new DoSomethingThatSometimesFails() { Id = Guid.NewGuid() };

                        await messageSession.SendLocal(doFail).ConfigureAwait(false);
                        _logger.Info($"Created Command To Do Something that works sometimes With Id: {doFail.Id}.");
                        break;
                    case ConsoleKey.Q:
                        return;
                    default:
                        _logger.Error("Unknown key - try again or use a dialling wand.");
                        break;
                }
            }
        }

    }
}
