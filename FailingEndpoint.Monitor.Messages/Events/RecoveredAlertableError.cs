﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FailingEndpoint.Monitor.Messages.Events
{
    public sealed class RecoveredAlertableError : IAmARecoveredError
    {
        public string Id { get; set; }
    }
}
